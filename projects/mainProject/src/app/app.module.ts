import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ContentComponent } from './content/content.component';
import { SidenavService } from './services/sidenav.service';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { ToggleSidebarModule } from './modules/toggle-sidebar/toggle-sidebar.module';
import { HeaderModule } from './modules/header/header.module';
import { SidebarModule } from './modules/sidebar/sidebar.module';
import { AppRoutingModule } from './app-routing.module';
import { MailModelsComponent, AddComponent, UpdateComponent, DeleteComponent, ShowAllComponent, ShowModelComponent, MailService } from 'projects/mailing-module/src/public-api';
import { MatTableExporterModule } from 'mat-table-exporter';
import { NgxSummernoteModule } from 'ngx-summernote';


@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    DashboardModule,
    HeaderModule,
    SidebarModule,
    ToggleSidebarModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    HttpClientModule,
    NgxSummernoteModule,
    MatTableExporterModule,
  ],
  declarations: [
    AppComponent,
    SidenavComponent,
    MailModelsComponent,
    AddComponent,
    UpdateComponent,
    DeleteComponent,
    ShowAllComponent,
    ShowModelComponent,
  ],
  providers: [SidenavService, MailService],
  entryComponents: [
    AddComponent,
    UpdateComponent,
    DeleteComponent,
    ShowAllComponent,
    ShowModelComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
