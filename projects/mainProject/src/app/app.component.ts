import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { mainContentAnimation } from './animations';
import { SidenavService } from './services/sidenav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    mainContentAnimation(),
  ]
})
export class AppComponent  implements OnDestroy,OnInit {
  title = 'mainProject';

  mobileQuery: MediaQueryList;

  sidebarState: string;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private sidebarService: SidenavService,) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  toggle(){
    console.log('logged app');
    
    this.sidebarService.toggle();
    //this.menuState = this.menuState === 'out' ? 'in' : 'out';
    // with another component use the service this.sidenavService.toggle();
  }
  
  ngOnInit() {
    this.sidebarService.sidebarStateObservable$
      .subscribe((newState: string) => {
        this.sidebarState = newState;
        console.log('sidebarState'+this.sidebarState);
        
      });
  }
}
