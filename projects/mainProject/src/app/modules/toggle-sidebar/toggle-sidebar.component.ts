import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { SidenavService } from '../../services/sidenav.service';

@Component({
  selector: 'app-toggle-sidebar',
  templateUrl: './toggle-sidebar.component.html',
  styleUrls: ['./toggle-sidebar.component.scss']
})
export class ToggleSidebarComponent implements OnInit {

  icontoShow:string = '';
  iconBool:boolean =true
  constructor(
    private sidebarService: SidenavService,
  ) { }

  ngOnInit() { 
    console.log(this.sidebarService.getState());
    this.icontoShow = 'menu';
  }

  toggleSideNav() {
    this.sidebarService.toggle();
    this.iconBool = !this.iconBool
    this.iconBool  ? this.icontoShow='menu' : this.icontoShow='more_vert'
  }
}
