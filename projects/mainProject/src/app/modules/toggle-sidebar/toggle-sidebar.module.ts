import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToggleSidebarComponent } from './toggle-sidebar.component';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from '@angular/platform-browser';
@NgModule({
  declarations: [
    ToggleSidebarComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [
    ToggleSidebarComponent,
  ]
})
export class ToggleSidebarModule {

  constructor(private matIconRegistry: MatIconRegistry,private domSanitizer: DomSanitizer){
    this.matIconRegistry.addSvgIcon(
      "right",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/right.svg")
    );
  }
 }
