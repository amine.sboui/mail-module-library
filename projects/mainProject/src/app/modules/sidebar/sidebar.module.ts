import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { MatIconModule, MatDividerModule, MatListModule, MatGridListModule, MatMenuModule, MatCardModule } from '@angular/material';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    CommonModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
  ],
  exports: [
    SidebarComponent,
  ]
})
export class SidebarModule { }
