import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ChangeDetectorRef
} from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { MediaMatcher } from "@angular/cdk/layout";
import { SidenavService } from '../services/sidenav.service';
import { sidebarAnimation, iconAnimation, labelAnimation, nameAnimation, notifAnimation, notiffAnimation } from '../animations';

@Component({
  selector: "app-sidenav",
  templateUrl: "./sidenav.component.html",
  styleUrls: ["./sidenav.component.scss"],
  animations: [
    sidebarAnimation(),
    iconAnimation(),
    labelAnimation(),
    nameAnimation(),
    notifAnimation(),
    notiffAnimation()
  ]
})
export class SidenavComponent implements OnInit {
  title = "mainProject";
  sidebarState: string;
  panelOpenState = false;

  

  constructor(
    
    private sidenavService: SidenavService
  ) {
   
  }

  ngOnInit() {
    this.sidenavService.sidebarStateObservable$.
      subscribe((newState: string) => {
        this.sidebarState = newState;
      });
      this.panelOpenState = true
  }


}
