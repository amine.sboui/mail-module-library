# MailingModule

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.9.

# NgxSummernote
Summernote wysiwyg editor for Angular.

This project was generated with Angular CLI version 6.0.0.

projects contains ngx-summernote lib (see Angular CLI libraries generation).

src contains an example application. Run ng serve to test.

   # Installation
    Install ngx-summernote and dependencies:

    npm install --save ngx-summernote summernote jquery

    Compatibility:

    Angular	ngx-summernote
    8	0.7.x
    7	0.6.x
    6	0.5.4
   # Editor
    Add add JQuery and Summernote scripts and styles in angular.json file:

    {#  
        "styles": [
        ...
        "node_modules/summernote/dist/summernote-lite.css"
        ],
        "scripts": [
        ...
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/summernote/dist/summernote-lite.js"
        ]
    #}
## Code scaffolding

Run `ng generate component component-name --project mailingModule` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project mailingModule`.
> Note: Don't forget to add `--project mailingModule` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build mailingModule` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build mailingModule`, go to the dist folder `cd dist/mailing-module` and run `npm publish`.

## Running unit tests

Run `ng test mailingModule` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
