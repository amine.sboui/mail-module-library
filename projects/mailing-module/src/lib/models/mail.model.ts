export class MailModel {

    modelCode: object;
    modelSubject: string;
    preHeader: string;
    content: string;
    footer: string;
    attachments: object;
    
    constructor() {
    }

  }