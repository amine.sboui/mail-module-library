import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MailModel } from '../../models/mail.model';
import { MailService } from '../../services/mail.service';
import { SelectAttributs } from '../../models/selectAttributs'
import { DomSanitizer } from '@angular/platform-browser';
import { literalMap } from '@angular/compiler';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  mailModel: MailModel = new MailModel();
  progress: number = 0;
  form: FormGroup;
  
  /* SummerNote Config */
  config: any = {
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo'/*, 'codeBlock'*/]],
      ['font', ['bold', 'italic', 'underline', 'strikethrough'/*, 'superscript', 'subscript', */, 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
    ],
  };
  language: SelectAttributs[] = [
    { value: 'fr', viewValue: 'Francais' },
    { value: 'en', viewValue: 'Anglais' },
  ];
  checkedlanguage: SelectAttributs[] = [];
  company: SelectAttributs[] = [
    { value: 'WiDigital', viewValue: 'WiDigital' },
    { value: 'Renault', viewValue: 'Renault' },
    { value: 'Europcar', viewValue: 'Europcar' }
  ];
  application: SelectAttributs[] = [
    { value: 'Servicima', viewValue: 'Servicima' },
    { value: 'Luca', viewValue: 'Luca' },
  ];
  get sanitizedHtml() {
    return this.sanitizer.bypassSecurityTrustHtml(this.form.get('content').value);
  }
  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public mailSer: MailService,
    private sanitizer: DomSanitizer) {
    this.form = new FormGroup({
      content: new FormControl()
    });
  }

  ngOnInit() {
    this.form = this._formBuilder.group({
      code: ['', Validators.required],
      language: ['', Validators.required],
      companyName: ['', Validators.required],
      applicationName: ['', Validators.required],
      modelSubject: ['', Validators.required],
      preHeader: ['', Validators.required],
      content: ['', Validators.required],
      footer: ['', Validators.required],
    });
    this.form.controls['language'].disable();
  }
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);


  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }
  submit() {
    // emppty stuff
  }
  progressList = []
  codeM: {}
  existinglanguage = []
  onBlur(name: string) {
    console.log(name);
    if (!this.progressList.includes(name)) {
      this.progressList.push(name);
      this.progress = this.progress + 12.5;
      console.log(this.progress);
    }
    if (this.form.get('code').value != '' && this.form.get('companyName').value != '' && this.form.get('applicationName').value != '') {
      this.codeM = {
        code: this.form.get('code').value,
        companyName: this.form.get('companyName').value,
        applicationName: this.form.get('applicationName').value
      }
      this.mailSer.getAvailableLang(
        this.codeM
      ).subscribe(
        (response) => {
          console.log(response['models']);
          response['models'].forEach(element => {
            this.existinglanguage.push(element.modelCode.language)
          });
          console.log('existinglanguage', this.existinglanguage);
          this.language.forEach(element => {
            if (!this.existinglanguage.includes(element.value)) {
              this.checkedlanguage.push(element)
            }
          })
          console.log('this.checkedlanguage', this.checkedlanguage);
        },
        (e) => {
          console.log('errorr', e);
        },
      );
      this.form.controls['language'].enable();
    }
  }
  onTyping() {
    this.progress = this.progress + 12.5;
    console.log(this.progress);

  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    var regex = /"/gi;
    console.log(this.form.value);
    this.mailModel = {
      modelCode: {
        code: this.form.value.code,
        language: this.form.value.language,
        companyDetails: {
          companyName: this.form.value.companyName,
          companyContact: 'contact@widigital-group.com',
          companyAddress: '28 Avenue du 19 Mars 1962, Plaisir 78370, France',
          companyWebSite: 'www.widigital-group.com.',
          companyPhoneNumber: '+(33) 1 86 95 46 66'
        },
        applicationName: this.form.value.applicationName,
      },
      modelSubject: this.form.value.modelSubject,
      preHeader: this.form.value.preHeader,
      content: `<div>${(this.form.get('content').value).replace(regex, "'")}</div>`,
      footer: `<div>${(this.form.get('footer').value).replace(regex, "'")}</div>`,
      attachments: {},
    }
    console.log(this.mailModel);
    this.mailSer.create(this.mailModel);
    console.log(this.data)
  }
}
