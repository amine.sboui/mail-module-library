import { Component, OnInit, Inject } from '@angular/core';
import { SelectAttributs } from '../../models/selectAttributs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MailModel } from '../../models/mail.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddComponent } from '../add/add.component';
import { MailService } from '../../services/mail.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $;
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})

export class UpdateComponent implements OnInit {
  mailModel: MailModel = new MailModel();
  progress: number = 0;
  form: FormGroup;
  selectedlanguage = { value: '', viewValue: '' }
  selectedcompany = { value: '', viewValue: '' }
  selectedapplication = { value: '', viewValue: '' }
  checkedConter: string = "";
  checkedFooter: string = "";
  /* SummerNote Config */
  config: any = {
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo'/*, 'codeBlock'*/]],
      ['font', ['bold', 'italic', 'underline', 'strikethrough'/*, 'superscript', 'subscript', */, 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
    ],
  };
  language: SelectAttributs[] = [
    { value: 'fr', viewValue: 'Francais' },
    { value: 'en', viewValue: 'Anglais' },
  ];
  company: SelectAttributs[] = [
    { value: 'WiDigital', viewValue: 'WiDigital' },
    { value: 'Renault', viewValue: 'Renault' },
    { value: 'Europcar', viewValue: 'Europcar' }
  ];
  application: SelectAttributs[] = [
    { value: 'Servicima', viewValue: 'Servicima' },
    { value: 'Luca', viewValue: 'Luca' },
  ];
  get sanitizedHtml() {
    return this.sanitizer.bypassSecurityTrustHtml(this.form.get('content').value);
  }
  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<UpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public mailSer: MailService,
    private sanitizer: DomSanitizer) {
    this.form = new FormGroup({
      content: new FormControl(),
      footer: new FormControl()
    });
  }

  ngOnInit() {
    if (this.data.modelCode.language)
      this.form = this._formBuilder.group({
        code: ['', Validators.required],
        language: ['', Validators.required],
        companyName: ['', Validators.required],
        applicationName: ['', Validators.required],
        modelSubject: ['', Validators.required],
        preHeader: ['', Validators.required],
        content: [],
        footer: []
      });
    this.setSelectedLanguage();
  }
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);


  setSelectedLanguage() {
    console.log('bef', this.form.value);

    //const toSelect = this.language.find(c => c.value == this.data.modelCode.language);
    this.form.get('code').setValue(this.data.modelCode.code);
    this.form.get('modelSubject').setValue(this.data.modelSubject);
    this.form.get('preHeader').setValue(this.data.preHeader);
    console.log('contnet', this.data.content);
    $('#content').summernote('pasteHTML', this.data.content);
    $('#footer').summernote('pasteHTML', this.data.footer);
    this.language.forEach(element => {
      if (this.data.modelCode.language == element.value) {
        this.selectedlanguage.viewValue = element.viewValue
        this.selectedlanguage.value = element.value
      }
    });
    this.company.forEach(element => {
      if (this.data.modelCode.companyDetails.companyName == element.value) {
        this.selectedcompany.viewValue = element.viewValue
        this.selectedcompany.value = element.value
      }
    });
    this.application.forEach(element => {
      if (this.data.modelCode.applicationName == element.value) {
        this.selectedapplication.viewValue = element.viewValue
        this.selectedapplication.value = element.value
      }
    });
    console.log('agter', this.form.value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }


  onBlur() {
    console.log('onBlur !');
  }
  public confirmUpdate(): void {
    var regex = /"/gi;
    if (this.form.get('content').value == null) {
      this.checkedConter = this.data.content
    }
    else { this.checkedConter = this.form.get('content').value }

    if (this.form.get('footer').value == null) {
      this.checkedFooter = this.data.footer
    }
    else { this.checkedFooter = this.form.get('footer').value }
    this.mailModel = {
      modelCode: {
        code: this.form.value.code,
        language: this.form.value.language,
        companyDetails: {
          companyName: this.form.value.companyName,
          companyContact: 'contact@widigital-group.com',
          companyAddress: '28 Avenue du 19 Mars 1962, Plaisir 78370, France',
          companyWebSite: 'www.widigital-group.com.',
          companyPhoneNumber: '+(33) 1 86 95 46 66'
        },
        applicationName: this.form.value.applicationName,
      },
      modelSubject: this.form.value.modelSubject,
      preHeader: this.form.value.preHeader,
      content: `<div>${(this.checkedConter).replace(regex, "'")}</div>`,
      footer: `<div>${(this.checkedFooter).replace(regex, "'")}</div>`,
      attachments: {},
    }
    console.log('mailModel', this.mailModel);
    console.log('fff', this.form.value);
    console.log('data', this.data._id);
    this.mailSer.update({ new: this.mailModel, old: this.data._id });
  }
}
