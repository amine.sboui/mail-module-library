import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-show-model',
  templateUrl: './show-model.component.html',
  styleUrls: ['./show-model.component.scss']
})
export class ShowModelComponent implements OnInit {
  
  content: any 
  footer: any
  
  constructor(
    sanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<ShowModelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    ) 
    { 
      this.content = sanitizer.bypassSecurityTrustHtml( this.data.mailModel.content);
      this.footer= sanitizer.bypassSecurityTrustHtml( this.data.mailModel.footer);
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
