import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MailModel } from '../models/mail.model';
import { BehaviorSubject } from 'rxjs';

const BACKEND_URL = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})

export class MailService {
  dataChange: BehaviorSubject<MailModel[]> = new BehaviorSubject<MailModel[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private http: HttpClient) { }


  get data(): MailModel[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  create(newMod: MailModel){
    return this.http.post(BACKEND_URL + '/createMailModel',
      newMod)
      .subscribe(
        data => {
          this.dialogData = newMod;
          console.log('POST Request is successful ', data);
        },
        error => {
          console.log('Error', error);
        }
      );

  }
  update(newoldModel){
    return this.http.put(BACKEND_URL + '/modelList',
    newoldModel)
      .subscribe(
        data => {
          this.dialogData = newoldModel.new;
          console.log('Update Request is successful ', data);
        },
        error => {
          console.log('Error', error);
        }
      );

  }
  getModel() {
    return this.http.get(BACKEND_URL + '/modelList?language=fr&code=RMP&applicationName=SERVICIMA&companyName=WiDigital')
  }
  getAllModels() {
    return this.http.get(BACKEND_URL + '/modelList')
  }
  getAvailableLang(code: any) {
    return this.http.get(BACKEND_URL + '/modelList?code='+code.code+'&applicationName='+code.applicationName+'&companyName='+code.companyName+'')
  }
  ArchiveModel(code: any) {
    console.log('delete', code)
    this.http.delete(BACKEND_URL + '/modelList?code='+code.code+'&applicationName='+code.applicationName+'&companyName='+code.companyName+'').subscribe(
      data => {
        //this.dialogData = code;
        console.log('Update Request is successful ', data);
      },
      error => {
        console.log('Error', error);
      }
    );
  }

}
