import { NgModule } from '@angular/core';
import { MailModelsComponent } from './mail-models/mail-models.component';
import { AddComponent } from './dialogs/add/add.component';
import { UpdateComponent } from './dialogs/update/update.component';
import { DeleteComponent } from './dialogs/delete/delete.component';
import { ShowAllComponent } from './dialogs/show-all/show-all.component';
import { ShowModelComponent } from './dialogs/show-model/show-model.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { MailService } from './services/mail.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatNativeDateModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MailModelsComponent,
    AddComponent,
    UpdateComponent,
    DeleteComponent,
    ShowAllComponent,
    ShowModelComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppMaterialModule,
    MatNativeDateModule,
    HttpClientModule,
  ],
  exports: [],
  entryComponents: [
    AddComponent,
    UpdateComponent,
    DeleteComponent,
    ShowAllComponent,
    ShowModelComponent
  ],
  providers: [MailService],
})
export class MailingModuleModule { }
