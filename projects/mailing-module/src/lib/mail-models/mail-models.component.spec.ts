import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailModelsComponent } from './mail-models.component';

describe('MailModelsComponent', () => {
  let component: MailModelsComponent;
  let fixture: ComponentFixture<MailModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
