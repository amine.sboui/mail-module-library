import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl } from '@angular/forms';
import { MailModel } from '../models/mail.model';
import { MailService } from '../services/mail.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { AddComponent } from '../dialogs/add/add.component';
import { DeleteComponent } from '../dialogs/delete/delete.component';
import { UpdateComponent } from '../dialogs/update/update.component';
import { ShowModelComponent } from '../dialogs/show-model/show-model.component';


declare var $;

export interface ModelsData {
  ModelSubject: string;
  PreHeader: string;
  Content: string;
  Footer: string;
}
@Component({
  selector: 'app-mail-models',
  templateUrl: './mail-models.component.html',
  styleUrls: ['./mail-models.component.scss']
})
export class MailModelsComponent implements OnInit {
  /* Subscribe */
  private ModelSub: Subscription;
  MailModelService: MailService | null;
  modelsList: MailModel[] = [];

  /* Tab */
  displayedColumns: string[] = ['Code', 'Language', 'Company', 'Application', 'Model', 'Status', 'Actions'];
  dataSource: MatTableDataSource<MailModel>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private mailSer: MailService, public dialog: MatDialog, ) {
  }


  ngOnInit() {
    this.getAllModel();

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  getAllModel() {
    this.ModelSub = this.mailSer.getAllModels().subscribe(
      (response) => {
        response['models'].forEach(model => {
          this.modelsList.push(model);
        });
        //this.modelsList.push(response['models']);
        console.log('testee', this.modelsList);
        this.display()
      },
      (e) => {
        console.log('errorr', e);
      },
    );
  }
  getModel() {
    this.ModelSub = this.mailSer.getModel().subscribe(
      (response) => {
        var HTMLstring = '<div><p>Hello, world</p><p>Summernote can insert HTML string</p></div>';
        console.log('cc', response['models'][0].content);
        $('#summernote').summernote('pasteHTML', response['models'].content);
        //this.modelList = response['projects'];
        console.log('111', response);
        console.log('222', response['models'][0]);
      },
      (e) => { 
        console.log('errorr', e);
      },
    );
  }
  display() {
    this.dataSource = new MatTableDataSource(this.modelsList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    console.log('datasui', this.dataSource)
  }
  mailModel: MailModel
  updateModel(mailModel: MailModel) {
    const dialogRef = this.dialog.open(UpdateComponent, {
      data: mailModel
    });

    dialogRef.afterClosed().subscribe(result => {
      this.modelsList = []
      this.getAllModel()
      this.paginator._changePageSize(this.paginator.pageSize);
    });
  }
  addNew() {
    console.log('blablablabla', MailModel);
    
    const dialogRef = this.dialog.open(AddComponent, {
      data: { mailModel: this.mailModel }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.modelsList = []
      this.getAllModel()
      this.paginator._changePageSize(this.paginator.pageSize);
    });
  }

  ArchiveModel(modelCode) {
    console.log('11', modelCode)
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: modelCode
    });

    dialogRef.afterClosed().subscribe(result => {
      this.modelsList = []
      this.getAllModel()
      this.paginator._changePageSize(this.paginator.pageSize);
    });
  }

  showModel(mailModel: MailModel) {
    this.dialog.open(ShowModelComponent, {
      data: { mailModel: mailModel }
    });
  }
}