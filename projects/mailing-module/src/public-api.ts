/*
 * Public API Surface of mailing-module
 */


export * from './lib/mailing-module.module';
export * from './lib/dialogs/add/add.component';
export * from './lib/dialogs/update/update.component';
export * from './lib/dialogs/show-all/show-all.component';
export * from './lib/dialogs/show-model/show-model.component';
export * from './lib/dialogs/delete/delete.component';
export * from './lib/services/mail.service';
export * from './lib/mail-models/mail-models.component'
